//
//  ViewController.m
//  SDWebImageVSYYWebImage
//
//  Created by 刘德平 on 2017/3/20.
//  Copyright © 2017年 刘德平. All rights reserved.
//

#import "ViewController.h"
#import "ExampleTableViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ExampleTableViewController *vc = [ExampleTableViewController new];
    [self pushViewController:vc animated:YES];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
